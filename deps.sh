#/bin/sh -vex

# Install the necessary build tools

apt-get update
apt-get install -y git git-buildpackage
apt-get install -y golang dh-golang
apt-get install -y golang-any

# Install already-packaged library dependencies.

# Packaged dependencies for golang-gopkg-square-go-jose.v2
apt-get install -y golang-github-stretchr-testify-dev golang-golang-x-crypto-dev

# Packaged dependencies for golang-gopkg-hlandau-acmeapi.v2
apt-get install -y golang-github-hlandau-goutils-dev golang-github-hlandau-xlog-dev golang-github-peterhellberg-link-dev golang-gopkg-square-go-jose.v1-dev

# Packaged dependencies for acmetool
apt-get install -y dh-apache2 golang-github-coreos-go-systemd-dev golang-github-hlandau-dexlogconfig-dev golang-github-jmhodges-clock-dev golang-github-mitchellh-go-wordwrap-dev golang-gopkg-alecthomas-kingpin.v2-dev golang-gopkg-cheggaaa-pb.v1-dev golang-gopkg-hlandau-easyconfig.v1-dev golang-gopkg-hlandau-service.v2-dev golang-gopkg-hlandau-svcutils.v1-dev golang-gopkg-tylerb-graceful.v1-dev libcap-dev
