#/bin/sh -vex

# Apply patches and invoke git-buildpackage to build
# packages from source.

# NB this will generally fail at the package-signing
# step as we neither update the changelog nor set up
# a signing key, but binary packages are still built.

PACKAGES="\
  golang-github-gofrs-uuid \
  golang-golang-x-xerrors \
  golang-github-google-go-cmp \
  golang-gopkg-square-go-jose.v2 \
  golang-gopkg-hlandau-acmeapi.v2 \
  acmetool"

for pkg in ${PACKAGES}; do
  if ! test -d ${pkg}; then
    echo "Can't find ${pkg} repo. Please run ./fetch.sh."
    exit 1
  fi
  pushd ${pkg}
  if test ${pkg} = "golang-gopkg-square-go-jose.v2"; then
    git checkout -b debian/buster
    git am ../0001-Branch-for-buster-backport.patch
    git am ../0002-Drop-dephelper-compat-dependency-to-version-12.patch
  elif test ${pkg} = ""acmetool; then
    git checkout -b debian/buster
    git am ../0001-Branch-for-buster-backport.patch
    git am ../0002-Skip-stripping-debug-info.patch
  fi
  gbp buildpackage
  popd
  dpkg -i ${pkg}*.deb
done
