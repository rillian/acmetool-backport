# acmetool backports

This is an experiment in backporting the acmetool 0.2 package from
Debian unstable to stable.

The 0.2 version, which has only been released as a beta by upstream,
adds support for the acme v2 api, which is now required by letsencrypt.
As such, the 0.67 package currently shipping in Debian 10 (stable)
isn't usable.

There's a `Dockerfile` describing a basic build environment.

    podman build -t acmetool-backport .
    podman run --rm -ti acmetool-backport

or if you have some other preferred environment, invoke the
scripts:

    ./deps.sh  # installs packaged dependencies
    ./fetch.sh # downloads unpackaged source

Then enter each project directory under `/src` in turn and fix
things up until `gbp buildpackage` works.

Otherwise, you can try invoking `./build.sh` which will patch, build,
and install the required packages according to the current plan
as documented in that script.

This is enough to get binary packages for testing, but package signing
fails because we don't provide a changelog entry or key environment,
and there may be subsequent issues with generating source packages.

As of the moment there are two changes needed to build on buster:

- `golang-gopkg-square-go-jose.v2` needs its dependency on
  `debhelper-compat` lowered from version 13 to version 12.
- `acmetool` needs an empty `override_dh_dwz` rule added to
  `debian/rules` to work around a failure stripping the
  debug tables.
