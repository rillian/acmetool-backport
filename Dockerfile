FROM debian:10

# Set up a working directory.
RUN mkdir -p /src
WORKDIR /src

# Install dependencies.
COPY deps.sh ./
RUN ./deps.sh
RUN apt-get clean

# Fetch unpackaged source repositories.
COPY fetch.sh ./
RUN ./fetch.sh

COPY *.patch ./
COPY build.sh ./
