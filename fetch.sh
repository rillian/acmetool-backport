#/bin/sh -vex

# Use git-buildpackage to fetch all the package repositories.
# It's important to use this wrapper since it sets up local
# tracking branches some packages expect for pristine-tar.

BASEREPO=https://salsa.debian.org/go-team/packages/
PACKAGES="\
  golang-github-gofrs-uuid \
  golang-golang-x-xerrors \
  golang-github-google-go-cmp \
  golang-gopkg-square-go-jose.v2 \
  golang-gopkg-hlandau-acmeapi.v2 \
  acmetool"

for pkg in ${PACKAGES}; do
  if test -d ${pkg}; then
    echo "${pkg} is already present."
  else
    gbp clone ${BASEREPO}${pkg}
  fi
done
